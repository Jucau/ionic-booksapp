import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiService } from '../../providers/api-service';
import { Page2 } from '../page2/page2';

/*
  Generated class for the Livre page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-livre',
  templateUrl: 'livre.html'
})
export class LivrePage {

    public book: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public apiService: ApiService) {
        this.book = {
                "title": navParams.get('title'),
                "description": navParams.get('description'),
                "code": navParams.get('code'),
                "id": navParams.get('id')
        };
        this.apiService = apiService;
    }

    ionViewDidLoad() {
        console.log('Hello LivrePage Page');
    }


    deleteBook() {
        this.apiService
            .deleteBook(this.book.id)
            .then(function(){
                this.navCtrl.push(Page2);
            });
    }
}
