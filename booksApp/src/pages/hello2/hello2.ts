import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { ApiService } from '../../providers/api-service';


/*
  Generated class for the Hello2 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-hello2',
  templateUrl: 'hello2.html'
})
export class Hello2Page {

  data: any;

  constructor(public navCtrl: NavController, public apiService:ApiService, public platform: Platform) {
      this.apiService = apiService;
  }

  ionViewDidLoad() {
    console.log('Hello Hello2Page Page');
  }

  ngOnInit() {
      this.platform.ready().then(() => {
          this.apiService.getUsers().then(data => {
              this.data = data;
              console.log(this.data);
          }).catch(err => {

          });
      });
  }

}
