import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AuthentificationPage } from '../authentification/authentification';
import { ApiService } from '../../providers/api-service';
import { LivrePage } from '../livre/livre';


@Component({
  selector: 'page-page2',
  templateUrl: 'page2.html'
})
export class Page2 {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, description: string, code: string, icon: string}>;
  data:any;
  read:any;
  readed:any;
  unread:any;
  unreaded:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public apiService: ApiService) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    this.storage = storage;
    this.apiService = apiService;

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.storage.get('id').then(
        res => {
            if (res !== null) { // si l'id n'est pas null
                this.apiService.getToken().then(function(result){this.getBooks(result, res);})
            } else { // sinon, retourne t'authentifier
                this.flushStorage();
            }
        }
    );

  }

  getBooks(id) {
      this.apiService.getToken().then(function(result){
              this.apiService.getBooks(result, id).then(data => {
              this.data = data;
              this.read = this.data.message['read'];
              this.unread = this.data.message['unread'];
              this.readed = [];
              this.unreaded = [];
              for (let i = 0; i < this.read.length; i++) {
                this.readed.push({
                  id: this.read[i]._id,
                  title: this.read[i].bookTitle,
                  description: this.read[i].bookDescription,
                  icon: this.icons[Math.floor(Math.random() * this.icons.length)]
                });
              }
              for (let i = 0; i < this.unread.length; i++) {
                this.unreaded.push({
                  id : this.unread[i]._id,
                  title: this.unread[i].bookTitle,
                  description: this.unread[i].bookDescription,
                  code: this.unread[i].bookCode,
                  icon: this.icons[Math.floor(Math.random() * this.icons.length)]
                });
              }
              console.log(this.unreaded);
          })
      }).catch(err => {
          console.log('erreur au chargement de l\'authentification');
      });
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(LivrePage, {
      id: item.id,
      title: item.title,
      description: item.description,
      code: item.code
    });
  }

  flushStorage() {
      this.storage.clear().then(() => this.navCtrl.setRoot(AuthentificationPage));
  }

}
