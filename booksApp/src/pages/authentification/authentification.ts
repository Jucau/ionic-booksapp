import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { ApiService } from '../../providers/api-service';
import { Storage } from '@ionic/storage';
import { Page2 } from '../page2/page2';

/*
  Generated class for the Authentification page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-authentification',
  templateUrl: 'authentification.html',
  providers: [ ApiService ]
})


export class AuthentificationPage {

    public data: any;

    constructor(
        public navCtrl: NavController,
        public apiService: ApiService,
        public platform: Platform,
        public storage: Storage
    ) {

    }

    ionViewWillEnter() {
        this.storage.clear();
        this.storage.get('token')
            .then(val =>
                {
                    var token = val;
                    if(token !== null) {
                        this.navCtrl.setRoot(Page2);
                    }
                }
            );
    }

    auth = {}
    // submit du formulaire d'authentification
    logForm() {
        this.apiService.postAuth(this.auth).then(data => {
            this.data = data;
            console.log(this.data);
            if(this.data.token !== undefined) {
                this.storage.set('token', this.data.token);
                this.storage.set('id', this.data.id).then(() =>
                    this.navCtrl.setRoot(Page2)
                );
            }
        }).catch(err => {
            console.log('erreur au chargement de l\'authentification');
        });
    }


}
