import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';

/*
  Generated class for the ApiService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ApiService {

    public result: any;

    constructor(public http: Http, public storage: Storage, public navCtrl: NavController, public requestOptions: RequestOptions) {
        console.log('Passage dans le provider ApiService');
        this.storage = storage;
    }

    getToken() {
        return new Promise(resolve => {
            this.storage.get('token').then(val => console.log(val));
        });
    }

    // send authentification
    postAuth(auth) {
        return new Promise(resolve => {
            this.http.post('http://localhost:3000/login', auth)
            .map(result => result.json())
            .subscribe(result => {
                this.result = result;
                resolve(this.result);
            })
        });
    }

    // afficher les infos utilisateur
    getUser(id) {
        return new Promise(resolve => {
            this.http.get('http://localhost:3000/users/' + id)
            .map(result => result.json())
            .subscribe(result => {
                this.result = result;
                resolve(this.result);
            })
        });
    }

    // afficher la liste des utilisateurs
    getUsers() {
        return new Promise(resolve => {
            this.http.get('http://localhost:3000/users')
            .map(result => result.json())
            .subscribe(result => {
                this.result = result;
                resolve(this.result);
            })
        });
    }

    getBooks(token,id) {
        var headers = new Headers({ 'Accept': 'application/json' });
        headers.append('x-access-token', String(token));

        return new Promise(resolve => {
            this.http.get('http://localhost:3000/users/' + id, { headers: headers })
            .map(result => result.json())
            .subscribe(result => {
                this.result = result;
                console.log(this.result);
                resolve(this.result);
            })
        });
    }

    deleteBook(id) {
        return new Promise(resolve => {
            this.http.delete('http://localhost:3000/books/' + id)
            .map(result => result.json())
            .subscribe(result => {
                this.result = result;
                console.log(this.result);
                resolve(this.result);
            })
        });
    }


}
