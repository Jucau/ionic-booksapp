import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Storage } from '@ionic/storage';


// import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { Hello2Page } from '../pages/hello2/hello2';
import { AuthentificationPage } from '../pages/authentification/authentification';
import { LivrePage } from '../pages/livre/livre';

// Create config options (see ILocalStorageServiceConfigOptions) for deets:

@NgModule({
  declarations: [
    MyApp,
    Page2,
    Hello2Page,
    AuthentificationPage,
    LivrePage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page2,
    Hello2Page,
    AuthentificationPage,
    LivrePage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, Storage]
})
export class AppModule {}
