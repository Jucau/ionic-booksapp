import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Storage } from '@ionic/storage';

// -------------- Providers ---------------
import { ApiService } from '../providers/api-service';
// -------------- Providers ---------------

// import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { Hello2Page } from '../pages/hello2/hello2';
import { AuthentificationPage } from '../pages/authentification/authentification';
// import { LivrePage } from '../pages/livre/livre';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  // rootPage: any = Page1;
  rootPage: any = AuthentificationPage;

  pages: Array<{title: string, component: any}>;

  // constructor(public platform: Platform) {
  constructor(
      private platform: Platform,
      public storage: Storage
    ) {
    this.initializeApp();

    this.storage = storage;

    // used for an example of ngFor and navigation
    this.pages = [
    //   { title: 'Page One', component: Page1 },
      { title: 'Mes livres', component: Page2 },
      { title: 'Hello 2', component: Hello2Page }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  flushConnexion() {
      this.storage.clear().then(() => this.nav.setRoot(AuthentificationPage));
  }
}
